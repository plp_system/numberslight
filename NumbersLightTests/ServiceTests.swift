//
//  ServiceTests.swift
//  NumbersLightTests
//
//  Created by Pierre-Loup BARAZZUTTI on 25/04/2021.
//

import XCTest
import RxSwift
import RxTest
import RxBlocking
@testable import NumbersLight

class ServiceTests: XCTestCase {
  /// DisposeBag (RxSwift)
  var disposeBag: DisposeBag!

  override func setUpWithError() throws {
    disposeBag = DisposeBag()
  }

  override func tearDownWithError() throws {}

  func getNumbersLight() {
    let response = Service.shared.getNumberLights().toBlocking(timeout: 100000000).materialize()
    guard case .completed(_) = response else {
      XCTFail("Fail to get the json response")
      return
    }
  }

  func getDetails(name: String) {
    let response = Service.shared.getDetails(name: name).toBlocking(timeout: 100000000).materialize()
    guard case .completed(_) = response else {
      XCTFail("Fail to get the json response")
      return
    }
  }

  func testNumbersLight() throws {
    XCTAssertEqual(Service.shared.isOfflineMode, false)
    getNumbersLight()
    XCTAssertGreaterThan(Service.shared.numberLights.value.count, 0)
  }

  func testNumberLight() throws {
    getNumbersLight()
    let element = Service.shared.numberLights.value[Int(arc4random()) % Service.shared.numberLights.value.count]
    XCTAssertNotNil(Service.shared.numberLight(name: element.name))
  }

  func testGetDetails() throws {
    getNumbersLight()
    let element = Service.shared.numberLights.value[Int(arc4random()) % Service.shared.numberLights.value.count]
    getDetails(name: element.name)
    XCTAssertNotNil(element.text)
    XCTAssertNotNil(element.largeImage)
  }

  func testOfflineMode() throws {
    XCTAssertEqual(Service.shared.isOfflineMode, false)
    Service.shared.switchService()
    XCTAssertEqual(Service.shared.isOfflineMode, true)

    XCTAssertGreaterThan(Service.shared.numberLights.value.count, 0)
    getNumbersLight()
    XCTAssertGreaterThan(Service.shared.numberLights.value.count, 0)

    let element = Service.shared.numberLights.value[Int(arc4random()) % Service.shared.numberLights.value.count]
    getDetails(name: element.name)

    XCTAssertNotNil(element.text)
    XCTAssertNotNil(element.largeImage)
  }

  func testSwitchOnlineMode() throws {
    XCTAssertEqual(Service.shared.isOfflineMode, true)
    Service.shared.switchService()
    XCTAssertEqual(Service.shared.isOfflineMode, false)
    XCTAssertEqual(Service.shared.numberLights.value.count, 0)
  }
}
