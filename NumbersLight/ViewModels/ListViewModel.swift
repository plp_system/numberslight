//
//  ListViewModel.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import Foundation
import RxSwift
import RxRelay

class ListViewModel: NumbersLightViewModel {
  /// List of datas (using by the tableView)
  var datas = BehaviorRelay<[GenericData]>(value: [])

  /// Selected Name  to color the corresponding cell (for iPad or landscape iphone)
  var selectedName: String?

  /// Used to display the details view of the selected number
  var showDetails = PublishRelay<String>()

  /// Used to change the background of the selected cell (for iPad or landscape iphone)
  var allowSelection: Bool = false {
    didSet { createDatas() }
  }

  override init() {
    super.init()

    // Subscription to the model
    Service.shared.numberLights
      .subscribe(onNext: { [weak self] _ in
        self?.createDatas()
      })
      .disposed(by: disposeBag)
  }

  override func refresh() {
    wait.accept(true)
    Service.shared.getNumberLights()
      .subscribe(onCompleted: { [weak self] in
        self?.wait.accept(false)
      }, onError: { [weak self] _ in
        self?.showError()
      })
      .disposed(by: disposeBag)
  }

  /// Create the datas (Switch + List of numberLight)
  func createDatas() {
    // At the beginning when we receive the data for the first time,
    // we update the details part with the first item in the list (only for ipad or landscape iphone)
    if selectedName == nil, let name = Service.shared.numberLights.value.first?.name {
      loadForSplit(name: name)
    }

    var newDatas = [GenericData]()
    newDatas.append(SwitchData(title: Localize.string("offline_mode"), value: Service.shared.isOfflineMode,
                               valueChanged: { [weak self] _ in
                                Service.shared.switchService()
                                self?.loadForSplit(name: nil)
                                self?.refresh()
                               }))
    newDatas.append(contentsOf: Service.shared.numberLights.value.map { numberLight in
      NumberLightData(numberLight, isSelected: allowSelection && numberLight.name == selectedName) { [weak self] data in
        guard let numberLightData = data as? NumberLightData else { return }
        self?.load(name: numberLightData.name)
      }
    })
    datas.accept(newDatas)
  }

  /// Load the details part for ipad
  /// Keep in memory the selected name/number
  /// - Parameters:
  ///   - name: Number / name
  func loadForSplit(name: String?) {
    guard allowSelection else { return }
    showDetails.accept(name ?? "")
    selectedName = name
    createDatas()
  }

  /// Load the details part
  /// - Parameters:
  ///   - name: Number / name
  func load(name: String) {
    guard !allowSelection else {
      loadForSplit(name: name)
      return
    }
    selectedName = name
    showDetails.accept(name)
  }
}
