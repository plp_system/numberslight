//
//  DetailsViewModel.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import Foundation
import RxSwift
import RxRelay

class DetailsViewModel: NumbersLightViewModel {
  /// Number/name
  /// Used to refresh the view
  var name = BehaviorRelay<String>(value: "")

  // Informations for the view
  var numberLight: NumberLight? { Service.shared.numberLight(name: name.value) }
  var title: String { Localize.string("number", name.value) }
  var text: String { numberLight?.text ?? "" }
  var image: String { numberLight?.largeImage ?? "" }

  /// Load the number
  /// If we already have all the information, we do not make the request again
  /// - Parameters:
  ///   - name: Number / name
  func showNumber(name: String) {
    self.name.accept(name)
    guard let numberLight = Service.shared.numberLight(name: name) else { return }
    // If we don't have the large image, we run a request to get it
    if numberLight.largeImage == nil {
      wait.accept(true)
      refresh()
    } else {
      wait.accept(false)
    }
  }

  override func refresh() {
    guard Service.shared.numberLight(name: name.value) != nil else { return }
    Service.shared.getDetails(name: name.value)
      .subscribe(onCompleted: { [weak self] in
        self?.name.accept(self?.name.value ?? "")
        self?.wait.accept(false)
      }, onError: { [weak self] _ in
        self?.showError()
      }).disposed(by: disposeBag)
  }
}
