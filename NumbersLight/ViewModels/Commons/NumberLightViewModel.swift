//
//  NumberLightViewModel.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import Foundation
import RxSwift
import RxRelay

/// Main ViewModel, parent of all other viewModels  in the project
class NumbersLightViewModel {
  /// DisposeBag (RxSwift)
  let disposeBag = DisposeBag()

  /// Used to display a error popup
  var error = PublishRelay<ErrorAlert>()

  /// Used to display the no internet banner
  var hasInternet = BehaviorRelay<Bool>(value: true)

  /// Used to display the waiting view
  var wait = BehaviorRelay<Bool>(value: true)

  init() {
    Service.shared.hasInternet
      .distinctUntilChanged()
      .observe(on: MainScheduler.instance)
      .subscribe(onNext: { [weak self] value in
        self?.hasInternet.accept(value)

        // Refresh the content only if we have internet
        if value {
          self?.refresh()
        } else {
          self?.wait.accept(false)
        }
      })
      .disposed(by: disposeBag)
  }

  func refresh() {}

  /// Show an error
  /// - Parameters:
  ///   - title: Title of the popup
  ///   - message: Message of the popup
  ///   - buttons: Array of title and action of button
  func showError(title: String = Localize.string("generic_error_title"),
                 message: String = Localize.string("generic_error_message"),
                 completion: (() -> Void)? = nil) {
    error.accept(ErrorAlert(title: title, message: message, buttons: [Localize.string("ok"): completion]))
  }
}
