//
//  ErrorAlert.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import Foundation

class ErrorAlert {
  var title: String
  var message: String
  var buttons: [String: (() -> Void)?]

  init(title: String, message: String, buttons: [String: (() -> Void)?]) {
    self.title = title
    self.message = message
    self.buttons = buttons
  }
}
