//
//  DetailsViewController.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import UIKit
import RxSwift

class DetailsViewController: NumbersLightViewController {
  /// ViewModel
  let viewModel = DetailsViewModel()
  override var numbersLightViewModel: NumbersLightViewModel { viewModel }

  // Views
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var numberLabel: UILabel!

  override func viewDidLoad() {
    super.viewDidLoad()
    imageView.backgroundColor = .backgroundImage
    imageView.layer.cornerRadius = 8

    viewModel.name
      .observe(on: MainScheduler.instance)
      .withUnretained(self)
      .subscribe(onNext: { owner, name in
        owner.title = owner.viewModel.title
        owner.imageView.isHidden = name.isEmpty
        owner.imageView.load(string: owner.viewModel.image)
        owner.numberLabel.text = owner.viewModel.text
      })
      .disposed(by: disposeBag)
  }
}
