//
//  ListViewController.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import UIKit
import RxSwift

class ListViewController: NumbersLightViewController {
  /// ViewModel
  let viewModel = ListViewModel()
  override var numbersLightViewModel: NumbersLightViewModel { viewModel }

  // Views
  @IBOutlet weak var tableView: GenericTableView!
  @IBOutlet weak var emptyView: UIView!
  @IBOutlet weak var emptyLabel: UILabel!

  // Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    title = Localize.string("numberlight")
    emptyView.isHidden = true
    emptyLabel.text = Localize.string("empty_list")

    tableView.backgroundColor = .clear
    tableView.refreshControl = UIRefreshControl()
    tableView.refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
    tableView.tableFooterView = UIView()

    // Adapter will configure the delegate and the datasource of the tableview and subscribe to the viewmodel
    tableView.adapter = TableViewAdapter(datas: viewModel.datas)

    viewModel.datas
      .skip(1)
      .observe(on: MainScheduler.instance)
      .subscribe(onNext: { [weak self] datas in
        self?.emptyView.isHidden = datas.count > 1
        self?.tableView.refreshControl?.endRefreshing()
      })
      .disposed(by: disposeBag)

    viewModel.showDetails
      .observe(on: MainScheduler.instance)
      .subscribe(onNext: { [weak self] name in
        self?.showDetails(name: name)
      })
      .disposed(by: disposeBag)

    NotificationCenter.default.addObserver(self, selector: #selector(refreshSelection),
                                           name: UIDevice.orientationDidChangeNotification, object: nil)
    refreshSelection()
  }

  /// Refresh the details part of the splitViewController with the selected number
  /// - Parameters:
  ///   - name: Name / Number
  func showDetails(name: String) {
    guard let splitViewController = splitViewController as? NumbersLightSplitViewController,
          var navigation = splitViewController.detailsViewController,
          var details = splitViewController.detailsViewController else { return }

    /// iOS 14
    if let navigation = splitViewController.detailsViewController as? UINavigationController,
       let visibleController = navigation.visibleViewController {
      details = visibleController
    } else {
      navigation = UINavigationController(rootViewController: details)
    }

    guard let detailsView = details as? DetailsViewController else { return }
    detailsView.viewModel.showNumber(name: name)
    splitViewController.showDetailViewController(navigation, sender: nil)
  }

  /// Create a UIAlertController and display it
  /// Overrided to remove the refreshcontrol of the tableview
  /// - Parameters:
  ///   - error: Object containing all information of the popup (title, message and buttons with actions)
  override func showErrorAlert(error: ErrorAlert) {
    super.showErrorAlert(error: error)
    tableView.refreshControl?.endRefreshing()
    UIView.animate(withDuration: 0.25) { [weak self] in
      self?.tableView.contentOffset = .zero
    }
  }

  @objc func refreshSelection() {
    viewModel.allowSelection = splitViewController?.isSplitted ?? false
  }
}
