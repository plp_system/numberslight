//
//  NumbersLightViewController.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import UIKit
import RxSwift

/// Main ViewController, parent of all other viewcontrollers in the project
class NumbersLightViewController: UIViewController {
  /// DisposeBag (RxSwift)
  let disposeBag = DisposeBag()

  /// Banner to display if we have no internet (Displayed only on online mode)
  public lazy var topBanner: TopBanner = { TopBanner(view: view) }()

  /// ViewModel
  var numbersLightViewModel: NumbersLightViewModel { NumbersLightViewModel() }

  /// Used to display waiting view and remove it when we have a response
  var waitingView: UIView?

  override func viewDidLoad() {
    super.viewDidLoad()
    clearBackButton()

    // Manage no internet banner
    numbersLightViewModel.hasInternet
      .observe(on: MainScheduler.instance)
      .subscribe(onNext: { [weak self] value in
        value ? self?.topBanner.remove() : self?.topBanner.add()
      })
      .disposed(by: disposeBag)

    // Display an error when a response has failed
    numbersLightViewModel.error
      .observe(on: MainScheduler.instance)
      .subscribe(onNext: { [weak self] error in
        self?.removeWaitingView()
        self?.showErrorAlert(error: error)
      })
      .disposed(by: disposeBag)

    // Display or remove the waiting view
    numbersLightViewModel.wait
      .observe(on: MainScheduler.instance)
      .subscribe(onNext: { [weak self] wait in
        wait ? self?.showWaitingView() : self?.removeWaitingView()
      })
      .disposed(by: disposeBag)
  }

  @objc func refresh() {
    numbersLightViewModel.refresh()
  }

  func showWaitingView() {
    removeWaitingView()
    waitingView = WaitingView.showWaitingView(view: self.view)
  }

  func removeWaitingView() {
    waitingView?.removeFromSuperview()
  }

  /// Create a UIAlertController and display it
  /// - Parameters:
  ///   - error: Object containing all information of the popup (title, message and buttons with actions)
  func showErrorAlert(error: ErrorAlert) {
    let alert = UIAlertController(title: error.title, message: error.message, preferredStyle: .alert)
    error.buttons.forEach { button in
      alert.addAction(UIAlertAction(title: button.key, style: .default, handler: { _ in button.value?() }))
    }
    present(alert, animated: true, completion: nil)
  }
}
