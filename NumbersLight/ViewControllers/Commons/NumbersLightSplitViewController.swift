//
//  NumbersLightSplitViewController.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import UIKit

class NumbersLightSplitViewController: UISplitViewController {
  var detailsViewController: UIViewController?

  override func viewDidLoad() {
    super.viewDidLoad()
    delegate = self
    preferredDisplayMode = .allVisible
    detailsViewController = viewControllers.last
  }
}

extension NumbersLightSplitViewController: UISplitViewControllerDelegate {
  func splitViewController(_ splitViewController: UISplitViewController,
                           collapseSecondary secondaryViewController: UIViewController,
                           onto primaryViewController: UIViewController) -> Bool {
    return true
  }

  @available(iOS 14.0, *)
  func splitViewController(_ svc: UISplitViewController, topColumnForCollapsingToProposedTopColumn
                            proposedTopColumn: UISplitViewController.Column) -> UISplitViewController.Column {
    return .primary
  }
}
