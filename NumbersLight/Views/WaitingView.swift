//
//  WaitingView.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import UIKit

/// Used to wait when a request is running
/// Display a view above the other components with an activity indicator
class WaitingView: UIView {
  override init(frame: CGRect) {
    super.init(frame: frame)
    initialize()
  }
  required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

  let containerView = UIView.useConstraint
  let activityIndicator = UIActivityIndicatorView(style: .large).useConstraint

  func initialize() {
    addSubviews()
    configure()
    setupLayout()
  }

  func addSubviews() {
    addSubview(containerView)
    containerView.addSubview(activityIndicator)
  }

  func configure() {
    backgroundColor = UIColor.black.withAlphaComponent(0.25)
    containerView.backgroundColor = .backgroundPopup
    containerView.layer.cornerRadius = 8
    activityIndicator.startAnimating()
  }

  func setupLayout() {
    NSLayoutConstraint.activate([
      containerView.centerXAnchor.constraint(equalTo: centerXAnchor),
      containerView.centerYAnchor.constraint(equalTo: centerYAnchor),
      activityIndicator.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 32),
      activityIndicator.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -32),
      activityIndicator.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 32),
      activityIndicator.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -32)
    ])
  }

  static func showWaitingView(view: UIView) -> UIView {
    let waitingView = WaitingView.useConstraint
    view.addSubview(waitingView)
    NSLayoutConstraint.activate([
      waitingView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      waitingView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      waitingView.topAnchor.constraint(equalTo: view.topAnchor),
      waitingView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
    ])
    return waitingView
  }
}
