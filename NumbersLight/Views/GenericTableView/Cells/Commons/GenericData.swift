//
//  GenericData.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import Foundation

/// Main data used by the tableviewAdapter.
/// All datas displayed by the genericTableview must inherit this and override the first 2 parameters
class GenericData {
  /// Identifier of the cell, used by the tableView (Register / dequeue)
  var cellIdentifier: String? { nil }

  /// Type of the cell: Type based on GenericCell
  var cellType: GenericCell.Type? { nil }

  /// Action when we did a select on a cell (didSelect)
  var select: ((GenericData) -> Void)?
}
