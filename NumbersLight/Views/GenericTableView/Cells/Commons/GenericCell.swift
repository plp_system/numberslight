//
//  GenericCell.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import UIKit
import RxSwift

/// Main cell used by the tableviewAdapter.
/// Each data has its own cell
class GenericCell: UITableViewCell {
  /// Associated data
  var data: GenericData?

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    addSubviews()
    setupLayout()
  }
  required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }

  func addSubviews() {}
  func setupLayout() {}
  func configure(data: GenericData) {
    self.data = data
  }

  /// DisposeBag (RxSwift)
  var disposeBag = DisposeBag()

  override func prepareForReuse() {
    // We have to reset the disposebag each time we reuse a cell
    disposeBag = DisposeBag()
  }
}
