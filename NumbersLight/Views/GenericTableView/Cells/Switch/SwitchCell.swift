//
//  SwitchCell.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import UIKit
import RxCocoa

class SwitchCell: GenericCell {
  let titleLabel = UILabel.useConstraint
  let switchComponent = UISwitch.useConstraint

  override func addSubviews() {
    super.addSubviews()
    contentView.addSubview(titleLabel)
    contentView.addSubview(switchComponent)
  }

  override func setupLayout() {
    super.setupLayout()
    NSLayoutConstraint.activate([
      titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
      titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
      titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
      switchComponent.leadingAnchor.constraint(greaterThanOrEqualTo: titleLabel.trailingAnchor, constant: 8),
      switchComponent.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
      switchComponent.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor)
    ])
  }

  override func configure(data: GenericData) {
    super.configure(data: data)
    guard let switchData = data as? SwitchData else { return }
    selectionStyle = .none
    titleLabel.text = switchData.title
    switchComponent.setOn(switchData.value, animated: false)
    switchComponent.rx
      .controlEvent(.valueChanged)
      .withLatestFrom(switchComponent.rx.value)
      .subscribe(onNext: { [weak self] value in
        guard let switchData = self?.data as? SwitchData else { return }
        switchData.value = value
        switchData.valueChanged?(switchData)
      })
      .disposed(by: disposeBag)
  }
}
