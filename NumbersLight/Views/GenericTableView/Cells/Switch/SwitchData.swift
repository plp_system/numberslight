//
//  SwitchData.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import Foundation

class SwitchData: GenericData {
  /// Identifier of the cell, used by the tableView (Register / dequeue)
  override var cellIdentifier: String? { "SwitchCell" }

  /// Type of the cell: Type based on GenericCell
  override var cellType: GenericCell.Type? { SwitchCell.self }

  // Informations for the Switch (title + value)
  let title: String
  var value: Bool
  var valueChanged: ((SwitchData) -> Void)?

  init(title: String, value: Bool, valueChanged: ((SwitchData) -> Void)?) {
    self.title = title
    self.value = value
    self.valueChanged = valueChanged
  }
}
