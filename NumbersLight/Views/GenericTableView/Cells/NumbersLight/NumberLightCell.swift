//
//  NumberLightCell.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import UIKit

class NumberLightCell: GenericCell {
  static let previewSize: CGFloat = 30

  let titleLabel = UILabel.useConstraint
  let preview = UIView.useConstraint
  let previewImage = UIImageView.useConstraint

  override func addSubviews() {
    super.addSubviews()
    contentView.addSubview(titleLabel)
    contentView.addSubview(preview)
    preview.addSubview(previewImage)
  }

  override func setupLayout() {
    super.setupLayout()
    NSLayoutConstraint.activate([
      preview.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
      preview.topAnchor.constraint(greaterThanOrEqualTo: contentView.topAnchor, constant: 12),
      preview.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
      previewImage.widthAnchor.constraint(equalToConstant: NumberLightCell.previewSize),
      previewImage.heightAnchor.constraint(equalToConstant: NumberLightCell.previewSize),
      previewImage.leadingAnchor.constraint(equalTo: preview.leadingAnchor, constant: 6),
      previewImage.topAnchor.constraint(greaterThanOrEqualTo: preview.topAnchor, constant: 6),
      previewImage.centerYAnchor.constraint(equalTo: preview.centerYAnchor),
      previewImage.centerXAnchor.constraint(equalTo: preview.centerXAnchor),
      titleLabel.leadingAnchor.constraint(equalTo: previewImage.trailingAnchor, constant: 16),
      titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
      titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
    ])
  }

  override func configure(data: GenericData) {
    super.configure(data: data)
    guard let numberLightData = data as? NumberLightData else { return }
    titleLabel.text = numberLightData.title
    preview.backgroundColor = .backgroundImage
    preview.layer.cornerRadius = 8
    previewImage.contentMode = .scaleAspectFit
    previewImage.load(string: numberLightData.image)
    refreshBackground()
  }

  override func setHighlighted(_ highlighted: Bool, animated: Bool) {
    super.setHighlighted(highlighted, animated: animated)
    refreshBackground()
  }

  func refreshBackground() {
    guard let numberLightData = data as? NumberLightData else { return }
    var background: UIColor = .systemBackground
    if numberLightData.isSelected {
      background = .selected
    } else if isHighlighted {
      background = .highlighted
    }
    contentView.backgroundColor = background
    backgroundColor = background
  }
}
