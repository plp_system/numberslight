//
//  NumberLightData.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import Foundation

class NumberLightData: GenericData {
  /// Identifier of the cell, used by the tableView (Register / dequeue)
  override var cellIdentifier: String? { "NumberLightCell" }

  /// Type of the cell: Type based on GenericCell
  override var cellType: GenericCell.Type? { NumberLightCell.self }

  // Informations for the NumberLight (title, image, selection)
  let title: String
  let name: String
  let image: String
  var isSelected: Bool = false

  init(_ numberLight: NumberLight, isSelected: Bool, select: ((GenericData) -> Void)?) {
    name = numberLight.name
    title = Localize.string("number", name)
    image = numberLight.preview
    super.init()
    self.select = select
    self.isSelected = isSelected
  }
}
