//
//  GenericTableView.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import UIKit

class GenericTableView: UITableView {
  public var adapter: TableViewAdapter? {
    didSet { adapter?.tableView = self }
  }
}
