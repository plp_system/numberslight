//
//  TableViewAdapter.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import UIKit
import RxSwift
import RxRelay

/// Adapter which will manage all the parts of the tableview (register, reload, display, action)
class TableViewAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
  /// DisposeBag (RxSwift)
  let disposeBag = DisposeBag()

  weak var tableView: UITableView? {
    didSet { reload() }
  }

  /// Array of datas
  var datas: [GenericData] = [] {
    didSet { reload() }
  }

  public init(datas: BehaviorRelay<[GenericData]>) {
    super.init()
    datas
      .observe(on: MainScheduler.instance)
      .subscribe(onNext: { [weak self] datas in
        self?.datas = datas
      }).disposed(by: disposeBag)
  }

  func reload() {
    registerCells()
    tableView?.delegate = self
    tableView?.dataSource = self
    tableView?.reloadData()
  }

  /// Register each cell from the array of datas
  func registerCells() {
    datas.forEach {
      if let type = $0.cellType, let identifier = $0.cellIdentifier {
        tableView?.register(type, forCellReuseIdentifier: identifier)
      }
    }
  }

  /// Number of cells (datas)
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { datas.count }

  /// Create the cell depending of the data
  /// And configure it with the data
  public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let data = datas[indexPath.row]
    guard let identifier = data.cellIdentifier else { return UITableViewCell() }
    let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
    guard let genericCell = cell as? GenericCell else { return UITableViewCell() }
    genericCell.configure(data: data)
    return cell
  }

  /// Selection of the cell
  /// Call the select callback of the data
  public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let data = datas[indexPath.row]
    tableView.deselectRow(at: indexPath, animated: true)
    data.select?(data)
  }
}
