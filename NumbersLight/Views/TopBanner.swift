//
//  TopBanner.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import UIKit

class TopBanner {
  /// Height of the banner
  static let bannerHeight: CGFloat = 30
  static let margin: CGFloat = 5

  /// List of affected constraints (constraints .top)
  public var topConstraints: [NSLayoutConstraint] = []

  /// Blue Banner
  public var banner: UIView = { UIView.useConstraint }()

  /// Parent view
  /// View from ViewController
  let parentView: UIView

  /// All content views
  var contentView: UIView = { UIView.useConstraint }()
  var icon: UIImageView = { UIImageView.useConstraint }()
  var titleLabel: UILabel = { UILabel.useConstraint }()

  /// Current height of the banner
  var height: CGFloat = 0

  /// Title of the banner
  var title: String = Localize.string("no_internet")

  /// Top constraint
  public var top: NSLayoutConstraint?

  init(view: UIView) {
    parentView = view
  }

  public func add() {
    searchTopConstraint()
    remove()

    addSubviews()
    configure()
    setupLayout()

    newHeight(height: TopBanner.bannerHeight)
  }

  func addSubviews() {
    parentView.addSubview(banner)
    banner.addSubview(contentView)
    contentView.addSubview(titleLabel)
    contentView.addSubview(icon)
  }

  func configure() {
    banner.isHidden = false
    banner.backgroundColor = .warning
    titleLabel.textAlignment = .center
    titleLabel.textColor = .white
    titleLabel.numberOfLines = 1
    titleLabel.font = UIFont.systemFont(ofSize: 14)
    titleLabel.setContentHuggingPriority(UILayoutPriority(249), for: .horizontal)
    titleLabel.text = title
    icon.image = UIImage(systemName: "wifi.slash")
    icon.tintColor = .white
  }

  func setupLayout() {
    top = banner.topAnchor.constraint(equalTo: parentView.safeAreaLayoutGuide.topAnchor)
    top?.isActive = true

    NSLayoutConstraint.activate([
      banner.leadingAnchor.constraint(equalTo: parentView.safeAreaLayoutGuide.leadingAnchor),
      banner.trailingAnchor.constraint(equalTo: parentView.safeAreaLayoutGuide.trailingAnchor),
      banner.heightAnchor.constraint(equalToConstant: CGFloat(TopBanner.bannerHeight)),
      contentView.leadingAnchor.constraint(greaterThanOrEqualTo: banner.leadingAnchor, constant: 16),
      contentView.trailingAnchor.constraint(lessThanOrEqualTo: banner.trailingAnchor, constant: -16),
      contentView.centerYAnchor.constraint(equalTo: banner.centerYAnchor),
      contentView.centerXAnchor.constraint(equalTo: banner.centerXAnchor),
      titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
      titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
      icon.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
      icon.trailingAnchor.constraint(equalTo: titleLabel.leadingAnchor, constant: -4),
      icon.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
      icon.topAnchor.constraint(equalTo: contentView.topAnchor),
      icon.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
    ])
  }

  /// Set the new height
  /// Change all top constraints
  /// - Parameters:
  ///   - height: New height
  func newHeight(height: CGFloat) {
    let diff = height - self.height
    topConstraints.forEach { $0.constant += diff }
    self.height = height
  }

  /// Remove the banner
  public func remove() {
    newHeight(height: 0)
    banner.isHidden = true
  }

  /// Find all the top constraints that will be affected by the banner
  func searchTopConstraint() {
    topConstraints.removeAll()
    parentView.constraints.forEach {
      guard !isLinkSafeArea(constraint: $0) else { return }
      if ($0.firstAttribute == .top && $0.secondAttribute == .top)
          || ($0.firstItem is UILayoutSupport && $0.firstAttribute == .bottom)
          || ($0.secondItem is UILayoutSupport && $0.secondAttribute == .bottom) {
        topConstraints.append($0)
      }
    }
  }

  /// Check the constraint
  /// - Returns: True if the passed constraint is a constraint between the parent view and these safearea
  func isLinkSafeArea(constraint: NSLayoutConstraint) -> Bool {
    return (constraint.firstItem is UILayoutSupport && constraint.secondItem as? UIView == parentView)
      || (constraint.firstItem as? UIView == parentView && constraint.secondItem is UILayoutSupport)
      || (constraint.firstItem is UILayoutGuide && constraint.secondItem as? UIView == parentView)
      || (constraint.firstItem as? UIView == parentView && constraint.secondItem is UILayoutGuide)
  }
}
