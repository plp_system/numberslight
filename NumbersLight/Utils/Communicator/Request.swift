//
//  Request.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 25/04/2021.
//

import Foundation
import RxSwift

class Request {
  /// Method (GET, POST, PUT)
  let method: CommunicatorMethod

  /// Address of the request
  let address: String

  /// Parameters of the request
  let parameters: [String: String]?

  /// Body of the request
  let body: [String: Any]?

  /// Headers of the request
  let headers: [String: String]?

  var session: URLSession {
    let configuration = URLSessionConfiguration.default
    configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
    return URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
  }

  init(method: CommunicatorMethod, address: String, parameters: [String: String]? = nil,
       body: [String: Any]? = nil, headers: [String: String]? = nil) {
    self.method = method
    self.address = address
    self.parameters = parameters
    self.body = body
    self.headers = headers
  }

  /// Create the request with all variables of Request
  func createRequest() -> URLRequest {
    var logString = method.rawValue + " " + address

    var urlComponents = URLComponents(string: address)!
    if let parameters = parameters {
      logString += "\nParameters: \(parameters)"
      urlComponents.queryItems = []
      parameters.forEach { urlComponents.queryItems?.append(URLQueryItem(name: $0, value: $1)) }
      urlComponents.percentEncodedQuery = urlComponents.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
    }

    var request = URLRequest(url: urlComponents.url!)
    if let body = body {
      logString += "\nBody: \(body)"
      do { request.httpBody = try JSONSerialization.data(withJSONObject: body) } catch {}
    }

    request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
    request.setValue("application/json", forHTTPHeaderField: "Accept")
    request.httpMethod = method.rawValue

    if let headers = headers {
      logString += "\nHeaders: \(headers)"
      headers.forEach { request.setValue($1, forHTTPHeaderField: $0) }
    }
    print(Date(), "plp log", logString)
    return request
  }

  /// Launch the request
  /// - Returns: Single
  func run() -> Single<Any> {
    return Single.create { [weak self] single -> Disposable in
      guard let self = self else { return Disposables.create {} }
      let request = self.createRequest()

      let task = self.session.dataTask(with: request) { [weak self] (data, response, error) in
        guard let self = self else { return }
        guard !self.hasError(data: data, response: response, error: error), let myData = data else {
          single(.failure(error ?? NSError(domain: "", code: -1, userInfo: nil)))
          return
        }
        self.createLogRequest(data: data, response: response, error: error)
        single(.success(myData))
      }
      task.resume()
      return Disposables.create {}
    }
  }

  /// Verifiy if the response of the request is an error
  /// - Parameters:
  ///   - data: Data of the response
  ///   - response: Response of the request
  ///   - error: Optional Error of the request
  func hasError(data: Data? = nil, response: URLResponse?, error: Error?) -> Bool {
    guard error == nil else { return true }
    if let response = response as? HTTPURLResponse {
      if response.statusCode < 200 || response.statusCode >= 300 {
        return true
      }
    }
    return false
  }

  /// Display a log with the response
  /// - Parameters:
  ///   - data: Data of the request
  ///   - response: Reponse of the request
  ///   - error: Error of the request
  public func createLogRequest(data: Data? = nil, response: URLResponse?, error: Error?, url: URL? = nil) {
    var logString = method.rawValue + " " + address
    if let response = response as? HTTPURLResponse {
      logString += "\nResponse StatusCode: \(response.statusCode)"
    }
    if let data = data {
      let json = try? JSONSerialization.jsonObject(with: data, options: [])
      if let json = json {
        logString += "\nData: \(json)"
      } else {
        logString += "\nData (String) \(String(data: data, encoding: .utf8) ?? " -- ")"
      }
    }
    if let url = url { logString += "\nUrl: \(url)" }
    if let error = error {
      logString += "\nErreur: \(error.localizedDescription)"
    }
    print(Date(), "plp log", logString)
  }
}
