//
//  Communicator.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import Foundation
import RxSwift

/// Request method
public enum CommunicatorMethod: String {
  case get = "GET"
  case post = "POST"
  case put = "PUT"
}

class Communicator {
  /// List of current requests
  var requests = [Request]()

  /// Create a GET request
  /// - Parameters:
  ///   - address: Address of the request
  ///   - parameters: Parameters of the request
  ///   - body: Body of the request
  ///   - headers: Headers of the request
  /// - Returns: Single
  func get(address: String, parameters: [String: String]? = nil, body: [String: Any]? = nil,
           headers: [String: String]? = nil) -> Single<Any> {
    request(method: .get, address: address, parameters: parameters, body: body, headers: headers)
  }

  /// Launch the request
  /// - Parameters:
  ///   - method: Method (GET, POST, PUT)
  ///   - address: Address of the request
  ///   - parameters: Parameters of the request
  ///   - body: Body of the request
  ///   - headers: Headers of the request
  /// - Returns: Single
  func request(method: CommunicatorMethod, address: String, parameters: [String: String]? = nil,
               body: [String: Any]? = nil, headers: [String: String]? = nil) -> Single<Any> {
    let request = Request(method: method, address: address, parameters: parameters, body: body, headers: headers)
    requests.append(request)
    return request.run()
      .do(onDispose: { [weak self] in
        self?.requests.removeAll { $0 === request }
      })
  }
}
