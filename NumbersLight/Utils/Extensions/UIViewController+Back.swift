//
//  UIViewController+Back.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import UIKit

extension UIViewController {
  /// Remove the back label for the next ViewController
  func clearBackButton() {
    let backButton = UIBarButtonItem()
    backButton.title = " "
    navigationItem.backBarButtonItem = backButton
  }
}
