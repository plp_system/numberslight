//
//  UISplitViewController+Utils.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 25/04/2021.
//

import UIKit

extension UISplitViewController {
  var isSplitted: Bool { viewControllers.count == 2 }
}
