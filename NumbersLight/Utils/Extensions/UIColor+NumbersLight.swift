//
//  UIColor+NumbersLight.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import UIKit

extension UIColor {
  static var backgroundPopup = UIColor(named: "backgroundPopup")!
  static var backgroundImage = UIColor(named: "backgroundImage")!
  static var highlighted = UIColor(named: "highlighted")!
  static var selected = UIColor(named: "selected")!
  static var warning = UIColor(named: "warning")!
}
