//
//  View+Constraint.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import UIKit

extension UIView {
  /// Allows to use constraints on the view
  /// Reduces the number of rows for views created by code
  var useConstraint: Self {
    self.translatesAutoresizingMaskIntoConstraints = false
    return self
  }

  /// Allows to use constraints on the view
  /// Reduces the number of rows for views created by code
  static var useConstraint: Self {
    let view = Self()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }
}
