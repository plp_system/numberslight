//
//  String+HTTP.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import Foundation

extension String {
  /// Check if the the string start with http/https
  var isHTTP: Bool { hasPrefix("http://") || hasPrefix("https://") }
}
