//
//  UIImage+Load.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import UIKit

/// Manage image cache
/// Avoid to download many time the same image
/// When we are not connected to internet, we still have the downloaded image
let imagesCache = NSCache<NSString, UIImage>()

extension UIImageView {
  /// Load a string that can be an url or a simply image (resource or system)
  /// - Parameters:
  ///   - string: url or name of the image
  func load(string: String) {
    // If we have the image in cache, we load the image
    if let image = imagesCache.object(forKey: string as NSString) {
      self.image = image
      return
    }

    // If the string is not an http or an url
    // We load the image from the resource or from the systemname
    guard string.isHTTP, let url = URL(string: string) else {
      if let image = UIImage(named: string) {
        self.image = image
      } else {
        self.image = UIImage(systemName: string)
      }
      return
    }
    load(url: url)
  }

  /// Load an url
  /// - Parameters:
  ///   - url: url of the image
  func load(url: URL) {
    // If we have the image in cache, we load the image
    if let image = imagesCache.object(forKey: url.absoluteString as NSString) {
      self.image = image
      return
    }

    self.image = nil

    // Download the image of the url
    DispatchQueue.global().async { [weak self] in
      // If we have a fail, we display nothing
      guard let data = try? Data(contentsOf: url), let image = UIImage(data: data) else {
        DispatchQueue.main.async { [weak self] in self?.image = nil }
        return
      }
      imagesCache.setObject(image, forKey: url.absoluteString as NSString)
      DispatchQueue.main.async { [weak self] in self?.image = image }
    }
  }
}
