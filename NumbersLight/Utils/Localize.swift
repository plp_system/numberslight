//
//  Localize.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import Foundation

/// Managing translations
/// Avoids having long lines just to retrieve translations
class Localize {
  static func string(_ key: String) -> String { NSLocalizedString(key, comment: "") }
  static func string(_ key: String, _ arguments: CVarArg...) -> String {
    String(format: NSLocalizedString(key, comment: ""), arguments: arguments)
  }
}
