//
//  NumbersLightConstants.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import Foundation

class NumbersLightConstants {
  static let listUrl = "http://dev.tapptic.com/test/json.php"
  static func numberUrl(_ name: String) -> String { "http://dev.tapptic.com/test/json.php?name=" + name }
}
