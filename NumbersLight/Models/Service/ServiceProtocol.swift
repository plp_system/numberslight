//
//  ServiceProtocol.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import Foundation
import RxSwift

protocol ServiceProtocol {
  /// Get the list of NumberLight
  /// - Returns: Completable (RxSwift)
  func getNumberLights() -> Completable

  /// Get the details of a NumberList
  /// - Parameters:
  ///   - name: Number / Name
  /// - Returns: Completable (RxSwift)
  func getDetails(name: String) -> Completable
}
