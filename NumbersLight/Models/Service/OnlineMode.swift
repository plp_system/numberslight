//
//  OnlineMode.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import Foundation
import RxSwift
import Reachability

class OnlineMode: ServiceProtocol {
  /// Communicator (Request)
  var communicator = Communicator()

  /// Reachability framework
  /// Used to know if the user is connected to the internet
  var reachability: Reachability!

  init() {
    do {
      reachability = try Reachability()
      try reachability.startNotifier()
    } catch {}
    NotificationCenter.default.addObserver(self, selector: #selector(checkForReachability),
                                           name: NSNotification.Name.reachabilityChanged, object: nil)
  }

  @objc func checkForReachability() {
    Service.shared.hasInternet.accept(reachability.connection != .unavailable)
  }

  /// Get the list of NumberLight
  /// - Returns: Completable (RxSwift)
  func getNumberLights() -> Completable {
    return communicator.get(address: NumbersLightConstants.listUrl)
      .do(onSuccess: { data in
        guard let data = data as? Data else { return }
        let numberLights = try JSONDecoder().decode([NumberLight].self, from: data)
        Service.shared.numberLights.accept(numberLights)
      }).asCompletable()
  }

  /// Get the details of a NumberList
  /// - Parameters:
  ///   - name: Number / Name
  /// - Returns: Completable (RxSwift)
  func getDetails(name: String) -> Completable {
    return communicator.get(address: NumbersLightConstants.numberUrl(name))
      .do(onSuccess: { data in
        guard let data = data as? Data else { return }
        let json = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]
        Service.shared.numberLight(name: name)?.largeImage = json?["image"] as? String
        Service.shared.numberLight(name: name)?.text = json?["text"] as? String
      }).asCompletable()
  }
}
