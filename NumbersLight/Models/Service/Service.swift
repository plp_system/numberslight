//
//  Service.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import Foundation
import RxSwift
import RxRelay
import Reachability

class Service {
  /// Defines if we are in offline mode or online mode
  /// Used online to switch between the two mode
  var isOfflineMode: Bool = false

  /// Separate the two modes (online or offline)
  var service: ServiceProtocol = OnlineMode()

  /// Filled either by reachability in online mode, or true in offline mode
  var hasInternet = BehaviorRelay<Bool>(value: true)

  /// List of NumberLight
  var numberLights = BehaviorRelay<[NumberLight]>(value: [])

  /// Singleton
  static let shared = Service()
  private init() {}

  func switchService() {
    isOfflineMode.toggle()
    if isOfflineMode {
      service = OfflineMode()

      // In offline, we remove the internet warning
      Service.shared.hasInternet.accept(true)
    } else {
      numberLights.accept([])
      service = OnlineMode()
    }
  }

  /// Get the NumberLight depending to the name
  /// - Parameters:
  ///   - name: Number / Name
  /// - Returns: NumberLight or nil
  func numberLight(name: String) -> NumberLight? { numberLights.value.first { $0.name == name } }

  // MARK: - Request
  /// Get the list of NumberLight
  /// - Returns: Completable (RxSwift)
  func getNumberLights() -> Completable { service.getNumberLights() }

  /// Get the details of a NumberList
  /// - Parameters:
  ///   - name: Number / Name
  /// - Returns: Completable (RxSwift)
  func getDetails(name: String) -> Completable { service.getDetails(name: name) }
}
