//
//  OfflineMode.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import Foundation
import RxSwift

class OfflineMode: ServiceProtocol {
  init() {
    // Creation of static content (20 NumberLight)
    Service.shared.numberLights.accept((0..<20).map { NumberLight(name: String($0)) })
  }

  /// Get the list of NumberLight
  /// - Returns: Completable (RxSwift)
  func getNumberLights() -> Completable {
    Service.shared.numberLights.accept(Service.shared.numberLights.value)
    return Completable.empty()
  }

  /// Get the details of a NumberList
  /// - Parameters:
  ///   - name: Number / Name
  /// - Returns: Completable (RxSwift)
  func getDetails(name: String) -> Completable { Completable.empty() }
}
