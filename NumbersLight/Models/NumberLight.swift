//
//  NumberLight.swift
//  NumbersLight
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import Foundation

class NumberLight: Decodable {
  let name: String
  let preview: String
  var largeImage: String?
  var text: String?

  enum CodingKeys: String, CodingKey {
    case name
    case preview = "image"
  }

  init(name: String) {
    self.name = name
    self.preview = name + ".circle"
    self.largeImage = name + ".circle"
    self.text = name
  }
}
