//
//  NumbersLightUITests.swift
//  NumbersLightUITests
//
//  Created by Pierre-Loup BARAZZUTTI on 24/04/2021.
//

import XCTest

class NumbersLightUITests: XCTestCase {

  override func setUpWithError() throws {
    // Put setup code here. This method is called before the invocation of each test method in the class.

    // In UI tests it is usually best to stop immediately when a failure occurs.
    continueAfterFailure = false

    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
  }

  override func tearDownWithError() throws {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
  }

  func testNumbersLightController() throws {
    // UI tests must launch the application that they test.
    let app = XCUIApplication()
    app.launch()

    XCTAssertGreaterThan(app.tables.cells.count, 1)
    XCTAssertNotNil(app.tables.cells.firstMatch.switches.firstMatch)
    app.tables.cells.firstMatch.switches.firstMatch.tap()
    XCTAssertGreaterThan(app.tables.cells.count, 1)
  }

  func testDetailsController() throws {
    let app = XCUIApplication()
    app.launch()
    XCTAssertGreaterThan(app.tables.cells.count, 1)
    app.tables.cells.element(boundBy: 1).tap()
  }
}
