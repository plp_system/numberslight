# NumbersLight #

Technical test for Bedrock.

### How to run the application? ###

1. Clone this repo
1. Open terminal and navigate to project folder
1. Run `pod install`
1. Open `NumbersLight.xcworkspace` and run the project on selected device or simulator
